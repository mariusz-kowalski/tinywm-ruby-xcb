tinywm-ruby-xcb
===============

This is [tinywm](https://github.com/mackstann/tinywm) implemented in Ruby.
Connection to X is done via XCB C library via FFI.

Checkout commits on master branch to see how to develop it step by step.

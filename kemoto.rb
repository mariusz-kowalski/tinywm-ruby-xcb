require_relative 'lib/xcb.rb'

connection = XCB.connect nil, 0
raise 'Connection to X server error.' unless connection[:has_error] == 0
setup = XCB.get_setup connection
iterator = XCB.setup_roots_iterator setup

screens = []
while iterator[:rem] > 0
  screens << iterator[:data]
  XCB.screen_next iterator.pointer
end

screens.map! { |screen_p| XCB::Screen.new screen_p }

screens.each do |screen|
  puts "screen: #{screen[:width_in_pixels]}x#{screen[:height_in_pixels]}"
end

# Cover only one screen for now
screen = screens.first
root = screen[:root]

XCB.grab_button(
  connection,
  0,
  root,
  XCB::EVENT_MASK_BUTTON_PRESS,
  :GRAB_MODE_ASYNC,
  :GRAB_MODE_ASYNC,
  root,
  XCB::NONE,
  XCB::BUTTON_RIGHT,
  XCB::MOD_MASK_R_ALT
)

XCB.grab_button(
  connection,
  0,
  root,
  XCB::EVENT_MASK_BUTTON_PRESS,
  :GRAB_MODE_ASYNC,
  :GRAB_MODE_ASYNC,
  root,
  XCB::NONE,
  XCB::BUTTON_LEFT,
  XCB::MOD_MASK_R_ALT
)
XCB.grab_key(
  connection,
  0,
  root,
  XCB::MOD_MASK_R_ALT,
  67, # Key 'F1' there should be beter solutoin for this ;)
  :GRAB_MODE_ASYNC,
  :GRAB_MODE_ASYNC
)

XCB.flush connection

def stack_above(connection, window)
  values = FFI::MemoryPointer.new(:uint32, 1)
  values.write_uint32 XCB::STACK_MODE_ABOVE
  XCB.configure_window(
    connection,
    window,
    XCB::CONFIG_WINDOW_STACK_MODE,
    values
  )
end

while true
  event = XCB.wait_for_event connection
  case event[:response_type] & ~0x80
  when XCB::BUTTON_PRESS
    btn_press = XCB::ButtonPressEvent.new event.pointer
    window = btn_press[:child]
    l_button = btn_press[:detail] == XCB::BUTTON_LEFT
    r_button = btn_press[:detail] == XCB::BUTTON_RIGHT
    r_alt = btn_press[:state] & XCB::MOD_MASK_R_ALT == XCB::MOD_MASK_R_ALT
    if window > 0 && (l_button || r_button) && r_alt
      stack_above connection, window
      geometry = XCB.get_geometry_reply(
        connection,
        XCB.get_geometry(connection, window),
        nil
      )
      if l_button
        offset_x = btn_press[:root_x] - geometry[:x]
        offset_y = btn_press[:root_y] - geometry[:y]
      elsif r_button
        offset_x = geometry[:x] + geometry[:width] - btn_press[:root_x]
        offset_y = geometry[:y] + geometry[:height] - btn_press[:root_y]
      end
      mask = XCB::EVENT_MASK_POINTER_MOTION | XCB::EVENT_MASK_BUTTON_RELEASE
      XCB.grab_pointer(
        connection,
        0,
        window,
        mask,
        :GRAB_MODE_ASYNC,
        :GRAB_MODE_ASYNC,
        root,
        XCB::NONE,
        XCB::CURRENT_TIME
      )
    end
  when XCB::MOTION_NOTIFY
    motion = XCB::MotionNotifyEvent.new event.pointer
    window = motion[:event]
    l_button = motion[:state] & XCB::BUTTON_MASK_LEFT == XCB::BUTTON_MASK_LEFT
    r_button = motion[:state] & XCB::BUTTON_MASK_RIGHT == XCB::BUTTON_MASK_RIGHT
    r_alt = motion[:state] & XCB::MOD_MASK_R_ALT == XCB::MOD_MASK_R_ALT
    if l_button && r_alt
      mask = XCB::CONFIG_WINDOW_X | XCB::CONFIG_WINDOW_Y
      values = FFI::MemoryPointer.new(:uint32, 2)
      values.write_array_of_uint32(
        [motion[:root_x] - offset_x, motion[:root_y] - offset_y]
      )
      XCB.configure_window(connection, window, mask, values)
    end
    if r_button && r_alt
      mask = XCB::CONFIG_WINDOW_HEIGHT | XCB::CONFIG_WINDOW_WIDTH
      values = FFI::MemoryPointer.new(:uint32, 2)
      values.write_array_of_uint32(
        [motion[:event_x] + offset_x, motion[:event_y] + offset_y]
      )
      XCB.configure_window(connection, window, mask, values)
    end
  when XCB::BUTTON_RELEASE
    XCB.ungrub_pointer connection, XCB::CURRENT_TIME
  when XCB::KEY_RELEASE
    key_release = XCB::KeyReleaseEvent.new event.pointer
    window = key_release[:child]
    r_alt = key_release[:state] & XCB::MOD_MASK_R_ALT == XCB::MOD_MASK_R_ALT
    stack_above(connection, window) if key_release[:detail] == 67 && r_alt
  end
  XCB.flush connection
end

XCB.disconnect connection

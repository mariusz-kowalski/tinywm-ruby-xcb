require 'ffi'

# Ruby binding to XCB
module XCB
  extend FFI::Library
  ffi_lib 'xcb'

  NONE             = 0
  COPY_FROM_PARENT = 0
  CURRENT_TIME     = 0
  NO_SYMBOL        = 0

  EVENT_MASK_NO_EVENT              =          0
  EVENT_MASK_KEY_PRESS             =          1
  EVENT_MASK_KEY_RELEASE           =          2
  EVENT_MASK_BUTTON_PRESS          =          4
  EVENT_MASK_BUTTON_RELEASE        =          8
  EVENT_MASK_ENTER_WINDOW          =         16
  EVENT_MASK_LEAVE_WINDOW          =         32
  EVENT_MASK_POINTER_MOTION        =         64
  EVENT_MASK_POINTER_MOTION_HINT   =        128
  EVENT_MASK_BUTTON_1_MOTION       =        256
  EVENT_MASK_BUTTON_2_MOTION       =        512
  EVENT_MASK_BUTTON_3_MOTION       =      1_024
  EVENT_MASK_BUTTON_4_MOTION       =      2_048
  EVENT_MASK_BUTTON_5_MOTION       =      4_096
  EVENT_MASK_BUTTON_MOTION         =      8_192
  EVENT_MASK_KEYMAP_STATE          =     16_384
  EVENT_MASK_EXPOSURE              =     32_768
  EVENT_MASK_VISIBILITY_CHANGE     =     65_536
  EVENT_MASK_STRUCTURE_NOTIFY      =    131_072
  EVENT_MASK_RESIZE_REDIRECT       =    262_144
  EVENT_MASK_SUBSTRUCTURE_NOTIFY   =    524_288
  EVENT_MASK_SUBSTRUCTURE_REDIRECT =  1_048_576
  EVENT_MASK_FOCUS_CHANGE          =  2_097_152
  EVENT_MASK_PROPERTY_CHANGE       =  4_194_304
  EVENT_MASK_COLOR_MAP_CHANGE      =  8_388_608
  EVENT_MASK_OWNER_GRAB_BUTTON     = 16_777_216

  BUTTON_INDEX_ANY = 0
  BUTTON_INDEX_1   = 1
  BUTTON_INDEX_2   = 2
  BUTTON_INDEX_3   = 3
  BUTTON_INDEX_4   = 4
  BUTTON_INDEX_5   = 5

  # helper consts, not form libxcb
  BUTTON_LEFT          = BUTTON_INDEX_1
  BUTTON_MIDDLE        = BUTTON_INDEX_2
  BUTTON_RIGHT         = BUTTON_INDEX_3
  BUTTON_ROLL_FORWARD  = BUTTON_INDEX_4
  BUTTON_ROLL_BACKWARD = BUTTON_INDEX_5

  MOD_MASK_SHIFT   =      1
  MOD_MASK_LOCK    =      2
  MOD_MASK_CONTROL =      4
  MOD_MASK_1       =      8
  MOD_MASK_2       =     16
  MOD_MASK_3       =     32
  MOD_MASK_4       =     64
  MOD_MASK_5       =    128
  MOD_MASK_ANY     = 32_768

  # helpers, not from libxcb
  MOD_MASK_R_ALT = MOD_MASK_1
  MOD_MASK_L_ALT = MOD_MASK_5
  MOD_MASK_SUPER = MOD_MASK_4

  STACK_MODE_ABOVE     = 0
  STACK_MODE_BELOW     = 1
  STACK_MODE_TOP_IF    = 2
  STACK_MODE_BOTTOM_IF = 3
  STACK_MODE_OPPOSITE  = 4

  KEY_PRESS      = 2
  KEY_RELEASE    = 3
  BUTTON_PRESS   = 4
  BUTTON_RELEASE = 5
  MOTION_NOTIFY  = 6
  # There is more in the libxcb

  CONFIG_WINDOW_X            =  1
  CONFIG_WINDOW_Y            =  2
  CONFIG_WINDOW_WIDTH        =  4
  CONFIG_WINDOW_HEIGHT       =  8
  CONFIG_WINDOW_BORDER_WIDTH = 16
  CONFIG_WINDOW_SIBLING      = 32
  CONFIG_WINDOW_STACK_MODE   = 64

  BUTTON_MASK_1 = 256
  BUTTON_MASK_2 = 512
  BUTTON_MASK_3 = 1024
  BUTTON_MASK_4 = 2048
  BUTTON_MASK_5 = 4096
  BUTTON_MASK_ANY = 32768

  BUTTON_MASK_LEFT = BUTTON_MASK_1
  BUTTON_MASK_MIDDLE = BUTTON_MASK_2
  BUTTON_MASK_RIGHT = BUTTON_MASK_3
  BUTTON_MASK_ROLL_FORWARD = BUTTON_MASK_4
  BUTTON_MASK_ROLL_BACKWARD = BUTTON_MASK_5

  typedef :uint32, :window_t
  typedef :uint32, :colormap_t
  typedef :uint32, :visualid_t
  typedef :uint32, :cursor_t
  typedef :uint8,  :keycode_t
  typedef :uint8,  :button_t
  typedef :uint32, :timestamp_t
  typedef :uint32, :drawable_t

  enum :GrabMode, [
    :GRAB_MODE_SYNC,
    :GRAB_MODE_ASYNC
  ]

  # xcb_connection_t
  class Connection < FFI::Struct
    layout :has_error, :int,
           :setup, :pointer,
           :fd, :int
           # I don't know how to implement types like pthread_mutex_t and don't
           # follow all the nested types. I hope that I can define only those
           # few first elements and it would work ;)
           #  :iolock, :pthread_mutex_t,
           #  :in, :_xcb_in,
           #  :out, :_xcb_out,
           #  :ext, :_xcb_ext,
           #  :xid, :_xcb_xid
  end

  # xcb_screen_iterator_t
  class ScreenIterator < FFI::Struct
    layout :data, :pointer,
           :rem, :int,
           :index, :int
  end

  # xcb_screen_t
  class Screen < FFI::Struct
    layout :root, :window_t,
           :default_colormap, :colormap_t,
           :white_pixel, :uint32,
           :black_pixel, :uint32,
           :current_input_masks, :uint32,
           :width_in_pixels, :uint16,
           :height_in_pixels, :uint16,
           :width_in_millimeters, :uint16,
           :height_in_millimeters, :uint16,
           :min_installed_maps, :uint16,
           :max_installed_maps, :uint16,
           :root_visual, :visualid_t,
           :backing_stores, :uint8,
           :save_unders, :uint8,
           :root_depth, :uint8,
           :allowed_depths_len, :uint8
  end

  # xcb_void_cookie_t
  class VoidCookie < FFI::Struct
    layout :sequence, :uint
  end

  #  xcb_generic_event_t;
  class GenericEvent < FFI::Struct
    layout :response_type, :uint8,
           :pad0,          :uint8,
           :sequence,      :uint16,
           :pad,           [:uint32, 7], # array of 7 :)
           :full_sequence, :uint32
  end

  # xcb_button_press_event_t;
  class ButtonPressEvent < FFI::Struct
    layout :response_type, :uint8,
           :detail,        :button_t,
           :sequence,      :uint16,
           :time,          :timestamp_t,
           :root,          :window_t,
           :event,         :window_t,
           :child,         :window_t,
           :root_x,        :int16,
           :root_y,        :int16,
           :event_x,       :int16,
           :event_y,       :int16,
           :state,         :uint16,
           :same_screen,   :uint8,
           :pad0,          :uint8
  end

  # xcb_get_geometry_reply_t;
  class GetGeometryReplay < FFI::Struct
    layout :response_type, :uint8,
           :depth,         :uint8,
           :sequence,      :uint16,
           :length,        :uint32,
           :root,          :window_t,
           :x,             :int16,
           :y,             :int16,
           :width,         :uint16,
           :height,        :uint16,
           :border_width,  :uint16,
           :pad0,          [:uint8, 2]
  end

  # xcb_generic_error_t;
  class GenericError < FFI::Struct
    layout :response_type, :uint8,
           :error_code,    :uint8,
           :sequence,      :uint16,
           :resource_id,   :uint32,
           :minor_code,    :uint16,
           :major_code,    :uint8,
           :pad0,          :uint8,
           :pad5,          [:uint32, 5],
           :full_sequence, :uint32
  end

  # xcb_motion_notify_event_t;
  class MotionNotifyEvent < FFI::Struct
    layout :response_type, :uint8,
           :detail,        :uint8, # nothing here !
           :sequence,      :uint16,
           :time,          :timestamp_t,
           :root,          :window_t,
           :event,         :window_t,
           :child,         :window_t,
           :root_x,        :int16,
           :root_y,        :int16,
           :event_x,       :int16,
           :event_y,       :int16,
           :state,         :uint16, # modifiers + buttons
           :same_screen,   :uint8,
           :pad0,          :uint8
  end

  # struct xcb_key_press_event_t
  class KeyPressEvent < FFI::Struct
    layout :response_type, :uint8,
           :detail,        :keycode_t,
           :sequence,      :uint16,
           :time,          :timestamp_t,
           :root,          :window_t,
           :event,         :window_t,
           :child,         :window_t,
           :root_x,        :int16,
           :root_y,        :int16,
           :event_x,       :int16,
           :event_y,       :int16,
           :state,         :uint16,
           :same_screen,   :uint8,
           :pad0,          :uint8
  end

  # same as KeyPressEvent
  KeyReleaseEvent = KeyPressEvent

  # xcb_connection_t *xcb_connect(const char *displayname, int *screenp);
  attach_function :connect, :xcb_connect, [:string, :int], Connection.by_ref
  # void xcb_disconnect(xcb_connection_t *c);
  attach_function :disconnect, :xcb_disconnect, [Connection.by_ref], :void
  # const struct xcb_setup_t *xcb_get_setup(xcb_connection_t *c);
  attach_function :get_setup, :xcb_get_setup, [Connection.by_ref], :pointer
  # xcb_screen_iterator_t xcb_setup_roots_iterator (const xcb_setup_t *R);
  attach_function :setup_roots_iterator, :xcb_setup_roots_iterator, [:pointer], ScreenIterator.by_value
  # void xcb_screen_next (xcb_screen_iterator_t *i  /**< */);
  attach_function :screen_next, :xcb_screen_next, [:pointer], :void

  # xcb_void_cookie_t
  # xcb_grab_button (xcb_connection_t *c  /**< */,
  #                  uint8_t           owner_events  /**< */,
  #                  xcb_window_t      grab_window  /**< */,
  #                  uint16_t          event_mask  /**< */,
  #                  uint8_t           pointer_mode  /**< */,
  #                  uint8_t           keyboard_mode  /**< */,
  #                  xcb_window_t      confine_to  /**< */,
  #                  xcb_cursor_t      cursor  /**< */,
  #                  uint8_t           button  /**< */,
  #                  uint16_t          modifiers  /**< */);
  attach_function :grab_button, :xcb_grab_button,
                  [
                    Connection.by_ref,
                    :uint8,     # owner_events
                    :window_t,  # grab_window
                    :uint16,    # event_mask
                    :GrabMode,  # pointer_mode
                    :GrabMode,  # keyboard_mode
                    :window_t,  # confine_to
                    :cursor_t,  # cursor
                    :uint8,     # button
                    :uint16     # modifiers
                  ],
                  VoidCookie.by_value

  # xcb_void_cookie_t
  # xcb_grab_key (xcb_connection_t *c  /**< */,
  #               uint8_t           owner_events  /**< */,
  #               xcb_window_t      grab_window  /**< */,
  #               uint16_t          modifiers  /**< */,
  #               xcb_keycode_t     key  /**< */,
  #               uint8_t           pointer_mode  /**< */,
  #               uint8_t           keyboard_mode  /**< */);
  attach_function :grab_key, :xcb_grab_key,
                  [
                    Connection.by_ref,
                    :uint8,      # event_owner
                    :window_t,   # grab_window
                    :uint16,     # modifiers
                    :keycode_t,  # keycode
                    :uint8,      # pointer_mode
                    :uint8       # keyboard_mode
                  ],
                  VoidCookie.by_value

  # int xcb_flush(xcb_connection_t *c);
  attach_function :flush, :xcb_flush, [Connection.by_ref], :int

  # xcb_void_cookie_t
  # xcb_configure_window (xcb_connection_t *c  /**< */,
  #                       xcb_window_t      window  /**< */,
  #                       uint16_t          value_mask  /**< */,
  #                       const uint32_t   *value_list  /**< */);
  attach_function :configure_window, :xcb_configure_window,
                  [
                    Connection.by_ref,
                    :window_t,
                    :uint16,
                    :pointer
                  ],
                  VoidCookie.by_value

  # xcb_generic_event_t *xcb_wait_for_event(xcb_connection_t *c);
  attach_function :wait_for_event, :xcb_wait_for_event,
                  [Connection.by_ref],
                  GenericEvent.by_ref

  # xcb_void_cookie_t
  # xcb_configure_window (xcb_connection_t *c  /**< */,
  #                       xcb_window_t      window  /**< */,
  #                       uint16_t          value_mask  /**< */,
  #                       const uint32_t   *value_list  /**< */);
  attach_function :configure_window, :xcb_configure_window,
                  [
                    Connection.by_ref,
                    :window_t,
                    :uint16,
                    :pointer
                  ],
                  VoidCookie.by_value

  # xcb_grab_pointer_cookie_t
  # xcb_grab_pointer (xcb_connection_t *c  /**< */,
  #                   uint8_t           owner_events  /**< */,
  #                   xcb_window_t      grab_window  /**< */,
  #                   uint16_t          event_mask  /**< */,
  #                   uint8_t           pointer_mode  /**< */,
  #                   uint8_t           keyboard_mode  /**< */,
  #                   xcb_window_t      confine_to  /**< */,
  #                   xcb_cursor_t      cursor  /**< */,
  #                   xcb_timestamp_t   time  /**< */);
  attach_function :grab_pointer, :xcb_grab_pointer,
                  [
                    Connection.by_ref,
                    :uint8,
                    :window_t,
                    :uint16,
                    :uint8,
                    :uint8,
                    :window_t,
                    :cursor_t,
                    :timestamp_t
                  ],
                  VoidCookie.by_value
                  # in the original is xcb_grab_pointer_cookie_t but it is the
                  # same, I'm using general one for now instead

  # xcb_void_cookie_t
  # xcb_ungrab_pointer (xcb_connection_t *c  /**< */,
  #                     xcb_timestamp_t   time  /**< */);
  attach_function :ungrub_pointer, :xcb_ungrab_pointer,
                  [Connection.by_ref, :timestamp_t],
                  VoidCookie.by_value

  # xcb_get_geometry_cookie_t
  # xcb_get_geometry (xcb_connection_t *c  /**< */,
  #                   xcb_drawable_t    drawable  /**< */);
  attach_function :get_geometry, :xcb_get_geometry,
                  [Connection.by_ref, :drawable_t],
                  VoidCookie.by_value

  # xcb_get_geometry_reply_t *
  # xcb_get_geometry_reply (xcb_connection_t           *c  /**< */,
  #                         xcb_get_geometry_cookie_t   cookie  /**< */,
  #                         xcb_generic_error_t       **e  /**< */);
  attach_function :get_geometry_reply, :xcb_get_geometry_reply,
                  [
                    Connection.by_ref,
                    VoidCookie.by_value,
                    GenericError.by_ref
                  ],
                  GetGeometryReplay.by_ref
end
